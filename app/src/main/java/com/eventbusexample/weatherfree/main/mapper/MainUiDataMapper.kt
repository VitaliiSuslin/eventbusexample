package com.eventbusexample.weatherfree.main.mapper

import com.eventbusexample.network.api.response.GetWeatherResponse
import com.eventbusexample.weatherfree.main.data.MainUiData

object MainUiDataMapper {

    fun mapToMapUiData(response: GetWeatherResponse): MainUiData {
        return MainUiData(response.toString())
    }
}