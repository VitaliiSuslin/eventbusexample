package com.eventbusexample.weatherfree.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eventbusexample.Injector
import com.eventbusexample.architecture.util.Event
import com.eventbusexample.architecture.viewmodel.BaseViewModel
import com.eventbusexample.weatherfree.main.data.MainUiData
import com.eventbusexample.weatherfree.main.mapper.MainUiDataMapper
import com.eventbusexample.weatherfree.main.repository.WeatherRepository
import kotlinx.coroutines.launch

class MainViewModel(
    private val weatherRepository: WeatherRepository = Injector.getWeatherRepository()
) : BaseViewModel() {

    companion object {
        private const val CITY_NAME = "Kyiv"
        private const val APP_ID = "7b21d526693f047991b3d94361893bd7"
    }

    private val dataSource by lazy { MutableLiveData<Event<MainUiData>>() }

    fun observeDataSource(): LiveData<Event<MainUiData>> = dataSource

    fun loadData() {
        launch(exceptionHandler(dataSource)) {
            onLoading(dataSource)
            val response = weatherRepository.getWeatherByCityName(CITY_NAME, APP_ID)
            val uiData = MainUiDataMapper.mapToMapUiData(response)
            onSuccess(dataSource, uiData)
        }
    }
}