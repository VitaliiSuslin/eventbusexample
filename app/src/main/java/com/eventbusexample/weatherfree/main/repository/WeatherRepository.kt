package com.eventbusexample.weatherfree.main.repository

import com.eventbusexample.network.api.response.GetWeatherResponse

interface WeatherRepository {
    suspend fun getWeatherByCityName(cityName: String, appId: String): GetWeatherResponse
}