package com.eventbusexample.weatherfree.main.data

data class MainUiData(
    val outputMessage: String
)