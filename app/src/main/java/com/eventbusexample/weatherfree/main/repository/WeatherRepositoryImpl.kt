package com.eventbusexample.weatherfree.main.repository

import com.eventbusexample.network.RestClient
import com.eventbusexample.network.api.response.GetWeatherResponse

class WeatherRepositoryImpl(
    private val resClient: RestClient
) : WeatherRepository {
    override suspend fun getWeatherByCityName(cityName: String, appId: String): GetWeatherResponse {
        return resClient.getWeatherByCityName(cityName, appId)
    }
}