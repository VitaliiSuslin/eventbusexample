package com.eventbusexample.weatherfree.main.activity

import android.os.Bundle
import androidx.activity.viewModels
import com.eventbusexample.architecture.view.BaseActivity
import com.eventbusexample.weatherfree.R
import com.eventbusexample.weatherfree.main.viewholder.MainViewHolder
import com.eventbusexample.weatherfree.main.viewmodel.MainViewModel

class MainActivity : BaseActivity(R.layout.activity_main) {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainViewHolder(this, viewModel)
    }
}