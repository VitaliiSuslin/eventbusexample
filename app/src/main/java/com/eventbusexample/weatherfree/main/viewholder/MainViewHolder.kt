package com.eventbusexample.weatherfree.main.viewholder

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import com.eventbusexample.architecture.util.Event
import com.eventbusexample.architecture.util.bindView
import com.eventbusexample.architecture.util.showSnackBar
import com.eventbusexample.architecture.view.LifecycleView
import com.eventbusexample.architecture.view.ViewManager
import com.eventbusexample.weatherfree.R
import com.eventbusexample.weatherfree.main.viewmodel.MainViewModel

class MainViewHolder(
    private val activity: AppCompatActivity,
    private val viewModel: MainViewModel
) : LifecycleView {
    override fun getLifecycleOwner(): LifecycleOwner = activity

    private val rootView: View by activity.bindView(R.id.rootView)
    private val outputContent: TextView by activity.bindView(R.id.outputContent)
    private val btnLoadData: Button by activity.bindView(R.id.btnLoadData)
    private val viewManager: ViewManager by lazy { ViewManager(rootView) }

    init {
        setupViewManager()
        setupListeners()
        observeDataSource()
    }

    private fun setupViewManager() {
        viewManager.setupViewManaging(R.id.progressView, R.id.outputContent, null)
    }

    private fun setupListeners() {
        btnLoadData.setOnClickListener { viewModel.loadData() }
    }

    private fun observeDataSource() {
        viewModel.observeDataSource().observeSource { event ->
            when (event) {
                is Event.Loading -> {
                    viewManager.showProgress()
                }
                is Event.Success -> {
                    outputContent.text = event.data.outputMessage
                    viewManager.showContent()
                }
                is Event.Failure -> {
                    event.failure.message?.run { rootView.showSnackBar(this) }
                    viewManager.showPlaceholder()
                }
            }
        }
    }
}