package com.eventbusexample

import android.app.Application

class App : Application() {
    init {
        Injector.setManagerProvider(ManagerProviderImpl())
    }
}