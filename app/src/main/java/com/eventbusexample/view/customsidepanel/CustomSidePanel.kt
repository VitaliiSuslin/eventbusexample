package com.eventbusexample.view.customsidepanel

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.RelativeLayout
import kotlin.math.abs

class CustomSidePanel(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs) {
    companion object {
        private const val SWIPE_THRESHOLD_X = 100f
        private const val SWIPE_THRESHOLD_Y = 100f
    }

    override fun onInterceptTouchEvent(event: MotionEvent?): Boolean {
        if (event != null
            && event.action == MotionEvent.ACTION_MOVE
            && abs(event.x) <= SWIPE_THRESHOLD_X
            && event.y >= SWIPE_THRESHOLD_Y
        ) {
            return true
        }
        return super.onInterceptTouchEvent(event)
    }

    fun setOnSwipeTouchListener(listener: OnSwipeTouchListener) {
        setOnTouchListener(listener)
    }

    class OnSwipeTouchListener(
        context: Context,
        val onSwipeLeft: () -> Unit = {},
        val onSwipeRight: () -> Unit = {}
    ) : OnTouchListener {

        private val detector by lazy {
            GestureDetector(context, CustomSwipe(object : CustomSwipe.SwipeListener {
                override fun onSwipeRight() {
                    onSwipeRight.invoke()
                }

                override fun onSwipeLeft() {
                    onSwipeLeft.invoke()
                }
            }))
        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            return detector.onTouchEvent(event)
        }

    }

    private class CustomSwipe(
        private val listener: SwipeListener
    ) : GestureDetector.SimpleOnGestureListener() {
        companion object {
            private const val TAG = "Swipe"
            private const val SWIPE_THRESHOLD_VELOCITY_X = 1000f
            private const val SWIPE_THRESHOLD_VELOCITY_Y = 70f
        }

        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent?,
            velocityX: Float,
            velocityY: Float
        ): Boolean {
            try {
                if (velocityX >= SWIPE_THRESHOLD_VELOCITY_X && abs(velocityY) > SWIPE_THRESHOLD_VELOCITY_Y) {
                    Log.i(TAG, "Left")
                    listener.onSwipeLeft()
                    return true
                }

                if (velocityX < SWIPE_THRESHOLD_VELOCITY_X && abs(velocityY) > SWIPE_THRESHOLD_VELOCITY_Y) {
                    Log.i(TAG, "Right")
                    listener.onSwipeRight()
                    return true
                }
            } catch (e: Exception) {
                Log.e(TAG, "Swipe error", e)
            }
            return false
        }

        interface SwipeListener {
            fun onSwipeLeft()
            fun onSwipeRight()
        }
    }
}

