package com.eventbusexample.network

import com.eventbusexample.network.api.WeatherApi
import com.eventbusexample.network.api.response.GetWeatherResponse
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RestClient : WeatherApi {

    companion object {
        const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
    }

    override suspend fun getWeatherByCityName(cityName: String, appId: String): GetWeatherResponse {
        return request(WeatherApi::class.java) {
            this.getWeatherByCityName(cityName, appId)
        }
    }

    private suspend fun <S : Any, R> request(
        serviceClass: Class<S>,
        call: suspend S.() -> R
    ): R {
        return call(retrofit().create(serviceClass))
    }

    private fun retrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(
                OkHttpClient.Builder()
                    .addNetworkInterceptor(NetworkInterceptor())
                    .addInterceptor(OkHttpProfilerInterceptor())
                    .build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    inner class NetworkInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            return chain.proceed(chain.request().newBuilder().build())
        }
    }
}