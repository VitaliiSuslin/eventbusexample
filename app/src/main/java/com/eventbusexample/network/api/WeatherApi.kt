package com.eventbusexample.network.api

import com.eventbusexample.network.api.response.GetWeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("weather")
    suspend fun getWeatherByCityName(
        @Query("q") cityName: String,
        @Query("appid") appId: String
    ): GetWeatherResponse
}