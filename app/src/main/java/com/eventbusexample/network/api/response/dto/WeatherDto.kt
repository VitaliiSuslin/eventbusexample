package com.eventbusexample.network.api.response.dto

import com.eventbusexample.network.util.SelfValidator
import com.google.gson.annotations.SerializedName

data class WeatherDto(
    @SerializedName("id")
    val id: Int,
    @SerializedName("main")
    val main: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("icon")
    val icon: String
) : SelfValidator {
    override fun validate(): Boolean { return id >= 0 }
}