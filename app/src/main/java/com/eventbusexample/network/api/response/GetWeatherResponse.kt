package com.eventbusexample.network.api.response

import com.eventbusexample.network.api.response.dto.CoordDto
import com.eventbusexample.network.api.response.dto.WeatherDto
import com.eventbusexample.network.util.BaseResponse
import com.eventbusexample.network.util.SelfValidator
import com.google.gson.annotations.SerializedName

data class GetWeatherResponse(
    @SerializedName("coord")
    val coordDto: CoordDto,
    @SerializedName("weather")
    val weathers: List<WeatherDto>
) : BaseResponse(), SelfValidator {
    override fun validate(): Boolean {
        return weathers.isNotEmpty()
    }
}