package com.eventbusexample.network.api.response.dto

import com.eventbusexample.network.util.SelfValidator
import com.google.gson.annotations.SerializedName

data class CoordDto(
    @SerializedName("lon")
    val lon: Double,
    @SerializedName("lat")
    val lat: Double
) : SelfValidator {
    override fun validate(): Boolean = true
}