package com.eventbusexample.network.util

interface SelfValidator {
    fun validate(): Boolean
}