package com.eventbusexample.network.util

import com.google.gson.annotations.SerializedName

abstract class BaseResponse(
    @SerializedName("cod")
    var code: Int? = null
) : SelfValidator