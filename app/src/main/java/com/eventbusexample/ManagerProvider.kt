package com.eventbusexample

import com.eventbusexample.weatherfree.main.repository.WeatherRepository

interface ManagerProvider {
    fun getWeatherRepository(): WeatherRepository
}