package com.eventbusexample.architecture.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import com.eventbusexample.architecture.util.Event
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel(), CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    /**
     * @param liveData [MutableLiveData] to take bus for sending data into main thread
     * @param data [T] to send existing loading data it into buss, by default it's null
     * */
    protected fun <T> onLoading(liveData: MutableLiveData<Event<T>>, data: T? = null) {
        sendToMainAsync(liveData, Event.Loading(data))
    }

    /**
     * @param liveData [MutableLiveData] to take bus for sending data into main thread
     * @param data [T] to send success event it into buss
     * */
    protected fun <T> onSuccess(liveData: MutableLiveData<Event<T>>, data: T) {
        sendToMainAsync(liveData, Event.Success(data))
    }

    /**
     * @param liveData [MutableLiveData] to take bus for sending data
     * @param throwable [Throwable] to send it into buss
     * */
    protected fun <T> onFailure(
        liveData: MutableLiveData<Event<T>>,
        throwable: Throwable
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            liveData.value = Event.Failure(throwable, liveData.value?.arguments)
        }
    }

    /**
     * @param liveData [MutableLiveData] to take bus for sending data
     * */
    protected fun <T> exceptionHandler(
        liveData: MutableLiveData<Event<T>>
    ): CoroutineExceptionHandler {
        return CoroutineExceptionHandler { _, throwable ->
            Log.e("Exception handler", "Error", throwable)
            onFailure(liveData, throwable)
        }
    }

    /**
     * @param liveData [MutableLiveData] to take bus for sending data into main thread
     * @param data [T] to send it into buss
     * */
    protected fun <T> sendToMainAsync(liveData: MutableLiveData<T>, data: T) {
        CoroutineScope(Dispatchers.Main).launch {
            liveData.value = data
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}