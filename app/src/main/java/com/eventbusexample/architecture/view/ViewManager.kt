package com.eventbusexample.architecture.view

import android.view.View
import androidx.annotation.IdRes
import androidx.core.view.isVisible

/**
 * Use [ViewManager] for useful view managing.
 * Should call [setupViewManaging] to setup views
 * Use fun for showing progress -> [showProgress]
 * Use fun for showing content -> [showContent]
 * Use fun for showing placeholder -> [showPlaceholder]
 * @param rootView [View] -> for connect manager to Root View
 * */
class ViewManager(private val rootView: View) {

    private var progressView: View? = null
    private var contentView: View? = null
    private var placeholder: View? = null

    /**
     * Setup ids for managing view
     *  @param progressResId [Int]
     *  @param contentResId [Int]
     *  @param placeholderResId [Int]
     * */
    fun setupViewManaging(
        @IdRes progressResId: Int?,
        @IdRes contentResId: Int?,
        @IdRes placeholderResId: Int?
    ) {
        progressResId?.let {
            progressView = rootView.findViewById(it)
        }

        contentResId?.let {
            contentView = rootView.findViewById(it)
        }

        placeholderResId?.let {
            placeholder = rootView.findViewById(it)
        }
    }

    fun showProgress() {
        contentView?.isVisible = false
        placeholder?.isVisible = false
        progressView?.isVisible = true
    }

    fun showContent() {
        progressView?.isVisible = false
        placeholder?.isVisible = false
        contentView?.isVisible = true
    }

    fun showPlaceholder() {
        contentView?.isVisible = false
        progressView?.isVisible = false
        placeholder?.isVisible = true
    }
}
