package com.eventbusexample.architecture.view

class LifecycleOwnerNotFoundException(message: String? = null): Throwable(message)