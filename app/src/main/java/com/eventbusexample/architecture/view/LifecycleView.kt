package com.eventbusexample.architecture.view

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.eventbusexample.architecture.util.Event

interface LifecycleView {
    fun getLifecycleOwner(): LifecycleOwner

    fun <T> LiveData<T>.observeSource(action: (T) -> Unit) {
        this.observe(getLifecycleOwner(), Observer {
            action.invoke(it)
        })
    }

    /**
     * Observe result and remove observer after result
     * */
    fun <T> LiveData<T>.observeSourceResult(action: (T) -> Unit) {
        this.observe(getLifecycleOwner(), Observer {
            action.invoke(it).also { this.removeObserver(action) }
        })
    }

    /**
     * Observe all changes while viewModel is alive
     * */
    fun <T> LiveData<Event<T>>.observeData(action: (Event<T>) -> Unit) {
        this.observe(getLifecycleOwner(), Observer {
            action.invoke(it)
        })
    }

    /**
     * Observe result and remove observer after result
     * */
    fun <T> LiveData<Event<T>>.observeResult(action: (Event<T>) -> Unit) {
        this.observe(getLifecycleOwner(), Observer { event ->
            when (event) {
                is Event.Loading -> action.invoke(event)
                is Event.Success -> action.invoke(event).also { this.removeObserver(action) }
                is Event.Failure -> action.invoke(event).also { this.removeObserver(action) }
            }
        })
    }
}