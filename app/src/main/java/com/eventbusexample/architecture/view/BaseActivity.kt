package com.eventbusexample.architecture.view

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.eventbusexample.architecture.util.Event

abstract class BaseActivity(@LayoutRes contentLayoutId: Int) : AppCompatActivity(contentLayoutId) {

    /**
     * Observe all changes while viewModel is alive
     * */
    fun <T> LiveData<T>.observeSource(action: (T) -> Unit) {
        this.observe(this@BaseActivity, Observer {
            action.invoke(it)
        })
    }

    /**
     * Observe result and remove observer after result
     * */
    fun <T> LiveData<T>.observeSourceResult(action: (T) -> Unit) {
        this.observe(this@BaseActivity, Observer {
            action.invoke(it).also { this.removeObserver(action) }
        })
    }

    /**
     * Observe all changes while viewModel is alive
     * */
    fun <T> LiveData<Event<T>>.observeData(action: (Event<T>) -> Unit) {
        this.observe(this@BaseActivity, Observer {
            action.invoke(it)
        })
    }

    /**
     * Observe result and remove observer after result
     * */
    fun <T> LiveData<Event<T>>.observeResult(action: (Event<T>) -> Unit) {
        this.observe(this@BaseActivity, Observer { event ->
            when (event) {
                is Event.Loading -> action.invoke(event)
                is Event.Success -> action.invoke(event).also { this.removeObserver(action) }
                is Event.Failure -> action.invoke(event).also { this.removeObserver(action) }
            }
        })
    }

    protected fun replaceFragment(fragment: Fragment, containerId: Int, needToAddToBackStack: Boolean = true, tag: String = fragment.javaClass.simpleName){
        with(supportFragmentManager.beginTransaction()){
            replace(containerId, fragment, tag)
            if(needToAddToBackStack){
                addToBackStack(tag)
            }
            commit()
        }
        supportFragmentManager.executePendingTransactions()
    }

    protected fun addFragment(fragment: Fragment, containerId: Int, needToAddToBackStack: Boolean = true, tag: String = fragment.javaClass.simpleName){
        with(supportFragmentManager.beginTransaction()){
            add(containerId, fragment, tag)
            if(needToAddToBackStack){
                addToBackStack(tag)
            }
            commit()
        }
        supportFragmentManager.executePendingTransactions()
    }
}