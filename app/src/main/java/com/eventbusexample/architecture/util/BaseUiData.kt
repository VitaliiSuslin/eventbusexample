package com.eventbusexample.architecture.util


interface BaseUiData
object EmptyUiData : BaseUiData

interface Action
object EmptyAction : Action