package com.eventbusexample.architecture.util

import android.view.View
import android.widget.Toast
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

fun View.showToast(@StringRes resourceId: Int, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this.context, resourceId, length).show()
}

fun View.showToast(message: String, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this.context, message, length).show()
}

fun View.showSnackBar(
    message: String,
    duration: Int = Snackbar.LENGTH_LONG
) {
    Snackbar.make(this, message, duration).show()
}

fun View.showSnackBar(
    @StringRes messageResId: Int,
    duration: Int = Snackbar.LENGTH_LONG
) {
    Snackbar.make(this, messageResId, duration).show()
}