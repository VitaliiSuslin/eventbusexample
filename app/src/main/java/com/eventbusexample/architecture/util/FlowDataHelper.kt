package com.eventbusexample.architecture.util

import android.util.Log
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlin.coroutines.CoroutineContext

abstract class FlowDataHelper : CoroutineScope {
    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    /**
     * @param flow [FlowCollector] to take bus for sending data into main thread
     * @param data [T] to send existing loading data it into buss, by default it's null
     * */
    protected fun <T> onLoading(sharedFlow: MutableSharedFlow<Event<T>>, data: T? = null) {
        CoroutineScope(Dispatchers.Main).launch {
            sharedFlow.tryEmit(Event.Loading(data))
        }
    }

    /**
     * @param flow [FlowCollector] to take bus for sending data into main thread
     * @param data [T] to send success event it into buss
     * */
    protected fun <T> onSuccess(sharedFlow: MutableSharedFlow<Event<T>>, data: T) {
        CoroutineScope(Dispatchers.Main).launch {
            sharedFlow.tryEmit(Event.Success(data))
        }
    }

    /**
     * @param flow [FlowCollector] to take bus for sending data
     * @param throwable [Throwable] to send it into buss
     * */
    protected fun <T> onFailure(
        sharedFlow: MutableSharedFlow<Event<T>>,
        throwable: Throwable
    ) {
        CoroutineScope(Dispatchers.Main).launch {
            sharedFlow.tryEmit(Event.Failure(throwable))
        }
    }

    /**
     * @param flow [FlowCollector] with [Event] and data [T] to take bus for sending data
     * */
    protected fun <T> exceptionHandler(
        flow: MutableSharedFlow<Event<T>>
    ): CoroutineExceptionHandler {
        return CoroutineExceptionHandler { _, throwable ->
            Log.d("Exception handler", "Error", throwable)
            onFailure(flow, throwable)
        }
    }
}