package com.eventbusexample.architecture.util

import android.app.Activity

fun <T> Activity.bindView(resId: Int): Lazy<T> {
    val view = this.findViewById(resId) as T
    return object : Lazy<T> {
        override val value: T
            get() = view

        override fun isInitialized(): Boolean = view != null

    }
}