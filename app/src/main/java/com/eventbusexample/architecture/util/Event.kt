package com.eventbusexample.architecture.util

sealed class Event<out T>(
    val arguments: T? = null,
    val throwable: Throwable? = null
) {
    private var hasBeenConsumed = false

    data class Loading<out T>(
        val data: T? = null
    ) : Event<T>(data)

    data class Success<out T>(
        val data: T
    ) : Event<T>(data)

    data class Failure<out T>(
        val failure: Throwable,
        val data: T? = null
    ) : Event<T>(data, failure)

    fun getContentIfNotConsumed(): T? {
        return if (hasBeenConsumed) {
            null
        } else {
            hasBeenConsumed = true
            arguments
        }
    }

    fun runBlockIfNotConsumed(block: (data: T?) -> Unit) {
        if (hasBeenConsumed) return
        hasBeenConsumed = true
        block.invoke(arguments)
    }
}