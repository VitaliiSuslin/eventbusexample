package com.eventbusexample.architecture.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

/**
 * Base RecyclerView.Adapter for all RecyclerView adapters in project.
 * Recycler use diffUtilCallback
 */
abstract class BaseRecyclerAdapter<T, VH : BaseAdapterViewHolder<T>>
    : RecyclerView.Adapter<VH>() {

    var recyclerView: RecyclerView? = null

    var currentList: MutableList<T> = mutableListOf()

    @CallSuper
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    @CallSuper
    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        (holder as? BaseAdapterViewHolder<T>)?.bindView(getItem(position))
    }

    override fun onBindViewHolder(holder: VH, position: Int, payloads: MutableList<Any>) {
        if(payloads.isEmpty()){
            super.onBindViewHolder(holder, position, payloads)
        } else {
            (holder as? BaseAdapterViewHolder<T>)?.bindView(getItem(position), payloads)
        }
    }

    protected fun createHolderView(parent: ViewGroup, @LayoutRes layoutId: Int): View {
        return LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
    }

    override fun getItemCount(): Int = currentList.size

    fun getItem(position: Int): T = currentList[position]

    fun add(item: T) {
        currentList.add(item)
        notifyItemInserted(currentList.size - 1)
    }

    fun addAll(items: List<T>) {
        this.currentList.addAll(items)
        notifyItemRangeInserted(itemCount - items.size, items.size)
    }

    fun insertAt(position: Int, item: T) {
        currentList.add(position, item)
        notifyItemInserted(position)
    }

    fun insertAll(position: Int, items: List<T>) {
        this.currentList.addAll(position, items)
        notifyItemRangeInserted(position, items.size)
    }

    fun replaceAt(position: Int, item: T) {
        currentList[position] = item
        notifyItemChanged(position)
    }

    fun replaceAll(items: List<T>) {
        val oldSize = this.currentList.size
        val newSize = items.size

        this.currentList = items.toMutableList()

        when {
            oldSize < newSize -> {
                notifyItemRangeChanged(0, oldSize)
                notifyItemRangeInserted(oldSize, newSize - oldSize)
            }
            oldSize > newSize -> {
                notifyItemRangeChanged(0, newSize)
                notifyItemRangeRemoved(newSize, oldSize - newSize)
            }
            oldSize == newSize -> notifyItemRangeChanged(0, oldSize)
        }
    }

    fun remove(item: T) {
        val position = currentList.indexOf(item)
        if (currentList.remove(item)) {
            notifyItemRemoved(position)
        }
    }

    fun removeAt(position: Int) {
        currentList.removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeAll(items: List<T>) {
        items.forEach {
            this.currentList.indexOf(it).let { position ->
                if (position != -1) {
                    removeAt(position)
                }
            }
        }
    }

    fun clear() {
        currentList.clear()
        notifyDataSetChanged()
    }

    fun submitList(items: MutableList<T>, detectMoves: Boolean = true) {
        DiffUtil.calculateDiff(diffUtilCallback(this.currentList, items), detectMoves).also {
            this.currentList = items
            it.dispatchUpdatesTo(this)
        }
    }

    private fun diffUtilCallback(oldList: List<T>, newList: List<T>): DiffUtil.Callback {
        return object : DiffUtil.Callback() {

            override fun getOldListSize(): Int = oldList.size

            override fun getNewListSize(): Int = newList.size

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return this@BaseRecyclerAdapter.areItemsTheSame(
                    oldItem = oldList[oldItemPosition],
                    newItem = newList[newItemPosition]
                )
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return this@BaseRecyclerAdapter.areContentsTheSame(
                    oldItem = oldList[oldItemPosition],
                    newItem = newList[newItemPosition]
                )
            }

            override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
                val diffBundle = this@BaseRecyclerAdapter.getChangePayload(
                    oldItem = oldList[oldItemPosition],
                    newItem = newList[newItemPosition]
                )
                return diffBundle ?: super.getChangePayload(oldItemPosition, newItemPosition)
            }
        }
    }

    abstract fun areItemsTheSame(oldItem: T, newItem: T): Boolean

    abstract fun areContentsTheSame(oldItem: T, newItem: T): Boolean

    open fun getChangePayload(oldItem: T, newItem: T): Bundle? = null
}
