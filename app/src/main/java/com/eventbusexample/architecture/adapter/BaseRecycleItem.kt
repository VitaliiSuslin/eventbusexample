package com.eventbusexample.architecture.adapter

interface BaseRecycleItem {
    fun isTheSameItem(other: BaseRecycleItem): Boolean
    fun isTheSameContent(other: BaseRecycleItem): Boolean
    fun getType(): Int
    fun getItemId(): String
}