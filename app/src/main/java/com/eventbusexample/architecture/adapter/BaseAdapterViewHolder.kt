package com.eventbusexample.architecture.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapterViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var item: T? = null
    var payloads: MutableList<Any>? = null
    open fun bindView(item: T){
        this.item = item
    }

    open fun bindView(item: T,  payloads: MutableList<Any>){
        this.item = item
        this.payloads = payloads
    }
}