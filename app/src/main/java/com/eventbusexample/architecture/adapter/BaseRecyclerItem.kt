package com.eventbusexample.architecture.adapter

import android.os.Bundle

interface BaseRecyclerItem {
    fun isTheSameItem(other: BaseRecyclerItem): Boolean
    fun isTheSameContent(other: BaseRecyclerItem): Boolean
    fun getType(): Int
    fun getItemId(): Any
    fun getChangePayload(other: BaseRecyclerItem): Bundle? = null
}