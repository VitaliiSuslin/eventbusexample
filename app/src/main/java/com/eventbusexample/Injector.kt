package com.eventbusexample

object Injector {
    private lateinit var managerProvider: ManagerProvider

    fun setManagerProvider(managerProvider: ManagerProvider) {
        synchronized(Injector::class) {
            this.managerProvider = managerProvider
        }
    }

    fun getWeatherRepository() = managerProvider.getWeatherRepository()
}