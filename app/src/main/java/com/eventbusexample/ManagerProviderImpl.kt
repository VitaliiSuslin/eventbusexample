package com.eventbusexample

import com.eventbusexample.network.RestClient
import com.eventbusexample.weatherfree.main.repository.WeatherRepository
import com.eventbusexample.weatherfree.main.repository.WeatherRepositoryImpl

class ManagerProviderImpl : ManagerProvider {
    private val restClient by lazy { RestClient() }

    private val weatherRepository by lazy { WeatherRepositoryImpl(restClient) }

    override fun getWeatherRepository(): WeatherRepository = weatherRepository
}